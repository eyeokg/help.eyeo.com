# This file is part of help.eyeo.com.
# Copyright [C] 2016 Eyeo GmbH
#
# help.eyeo.com is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# [at your option] any later version.
#
# help.eyeo.com is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with help.eyeo.com.  If not, see <http://www.gnu.org/licenses/>.

def get_inline_bg(filename):
  png = 'background-image: url(/dist/img/png/' + filename + '.png);'
  svg = 'background-image: linear-gradient(transparent, transparent), url(/dist/img/svg/' + filename + '.svg);'
  return png + svg
