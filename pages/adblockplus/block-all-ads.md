title=Block all ads
description=Browse the web in peace by blocking all ads, even those that apply to the Acceptable Ads criteria.
template=article
product_id=abp
category=Customization & Settings
popular=true
include_heading_level=h2

If you choose to block all ads, Adblock Plus will block annoying ads *and* ads that are considered nonintrusive (Acceptable Ads). To learn more about Acceptable Ads, visit the [Acceptable Ads webpage](https://acceptableads.com/en/).

<aside class="alert warning" markdown="1">
**Note**: By turning off the Acceptable Ads feature, website owners and content creators that abide by the Acceptable Ads criteria will lose ad revenue.
</aside>

<? include adblockplus/blockads ?>
