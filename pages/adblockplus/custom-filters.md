title=Add a custom filter
description=You can create your own filters and add them to Adblock Plus.
template=article
product_id=abp
category=Customization & Settings

Adblock Plus comes with a set of standard, pre-made filter lists. If you want further control over what you see and what you don't see when you browse the web, you can create your own custom filter(s) or add custom-made filters to Adblock Plus. [Learn how to write your own filters](adblockplus/how-to-write-filters)

<section class="platform-chrome" markdown="1">
## Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Create and edit your filter list* section.
3. Click **Start creating my filter list**.
4. Paste your custom filter list into the box.
5. Click **Save**.
</section>

<section class="platform-msedge" markdown="1">
## Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Create and edit your filter list* section.
3. Click **Start creating my filter list**.
4. Paste your custom filter list into the box.
5. Click **Save**.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Create and edit your filter list* section.
3. Click **Start creating my filter list**.
4. Paste your custom filter list into the box.
5. Click **Save**.
</section>

<section class="platform-opera" markdown="1">
## Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Create and edit your filter list* section.
3. Click **Start creating my filter list**.
4. Paste your custom filter list into the box.
5. Click **Save**.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Create and edit your filter list* section.
3. Click **Start creating my filter list**.
4. Paste your custom filter list into the box.
5. Click **Save**.
</section>