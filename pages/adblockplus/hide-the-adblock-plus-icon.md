title=Hide or show the Adblock Plus icon
description=Hide or show the Adblock Plus icon on your browser's toolbar.
template=article
product_id=abp
category=Customization & Settings

You can easily hide the Adblock Plus icon if you do not want it displayed on your toolbar.

<section class="platform-chrome" markdown="1">
## Chrome

### How to hide the icon {: #chrome-hide }

1. From the Chrome toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Hide in Chrome Menu**.
<br>The icon is removed from the toolbar.

### How to show the icon {: #chrome-show }

1. Click the **Chrome menu** icon.
2. Right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Show in Toolbar**.
<br>The icon now appears on the toolbar.
</section>

<section class="platform-msedge" markdown="1">
## Edge

### How to hide the icon {: #edge-hide }

1. From the Edge toolbar, right-click the **Adblock Plus** icon and select **Show next to address bar**.
<br>The icon is removed from the toolbar.

### How to show the icon {: #edge-show }

1. Click the **Edge menu** icon.
2. Right-click Adblock Plus and select **Show next to address bar**.
<br>The icon now appears on the toolbar.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

### How to hide the icon {: #firefox-hide }

1. From the Firefox toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Remove from Toolbar**.
<br>The icon is removed from the toolbar.

### How to show the icon {: #firefox-show }

1. Right-click (Windows) / control-click (Mac) the Firefox toolbar and select **Customize**.
<br>The *Customize Firefox* tab opens.
2. Select the **Adblock Plus** icon and drag it to the toolbar.
3. Drop the icon in your preferred location.
<aside class="alert info" markdown="1">
**Tip**: You can reorder the toolbar icons while the *Customize Firefox* tab is open.
</aside>
4. Click **Done**.
<br>The icon now appears on the toolbar.
</section>

<section class="platform-msie" markdown="1">
## Internet Explorer

### How to hide the icon {: #ie-hide }

1. From the Internet Explorer toolbar, right-click the tab bar (the empty area near the tabs) and select **Status bar**.
<br>The Status bar and the Adblock Plus icon disappear.

### How to show the icon {: #ie-show }

1. From the Internet Explorer toolbar, right-click the tab bar (the empty area near the tabs) and select **Status bar**.
<br>The Status bar and the Adblock Plus icon re-appear.
</section>

<section class="platform-opera" markdown="1">
## Opera

### How to hide the icon {: #opera-hide }

1. From the Opera toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Hide from toolbar**.
<br>The icon is removed from the toolbar.

### How to show the icon {: #opera-show }

#### Windows {: #opera-show-windows }

1. Click the **Opera** icon, hover over **Extensions** and select **Extensions**. 
<br>The *Extensions* tab opens.
2. Locate Adblock Plus and clear the check box labeled **Hide from toolbar**. 
<br>The icon now appears on the toolbar.
3. Close the tab.

#### Mac {: #opera-show-mac }

1. From the Mac menu bar, click **View** and select **Show Extensions**.
<br>The *Extensions* tab opens.
2. Locate Adblock Plus and clear the check box labeled **Hide from toolbar**.
<br>The icon now appears on the toolbar.
3. Close the tab.
</section>

<section class="platform-safari" markdown="1">
## Safari

### How to hide the icon {: #safari-hide }

1. Open Safari.
2. Hold down the Command key and drag the **Adblock Plus** icon from the Safari toolbar.

OR

1. From the Mac menu bar, click **Safari** and select **Preferences**.
2. From the *Extensions* tab, locate Adblock Plus and clear the check box labeled **ABP Control Panel**.
<br>The icon is removed from the toolbar.

### How to show the icon {: #safari-show }

1. From the Mac menu bar, click **Safari** and select **Preferences**.
2. From the *Extensions* tab, locate Adblock Plus and select the check box labeled **ABP Control Panel**.
<br>The icon now appears on the toolbar.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

### How to hide the icon {: #yandex-hide }

1. From the Yandex Browser toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Hide button**.
<br>The icon is removed from the toolbar.

### How to show the icon {: #yandex-show }

1. Click the **Yandex Browser menu** icon and select **Add-ons**.
<br>The *Add-ons* tab opens.
2. Locate Adblock Plus and click **More details**.
3. Click **Show button**.
<br>The icon now appears on the toolbar.
4. Close the tab.
</section>
