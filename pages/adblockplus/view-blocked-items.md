title=View a blocked element or unblock an element
description=Remove rules that you previously set or view items that you blocked with Adblock Plus.
template=article
product_id=abp
category=Customization & Settings

Adblock Plus allows you to customize on-page actions depending on the page you're viewing. Follow the steps below if you blocked an element and want to view it, add an exception rule, remove a rule that you previously set, or unblock the element.

<section class="platform-chrome" markdown="1">
## Chrome

### How to view a blocked element {: #chrome-view }

1. From the Chrome toolbar, click the **Chrome menu** icon.
2. Hover over **More Tools** and select **Developer Tools**.
3. Click the **>>** icon in the upper right corner and select **Adblock Plus**.

### How to add an exception OR unblock an element {: #chrome-unblock }

1. Follow the steps below *How to view a blocked element*.
2. From the drop-down menu, select **blocked**.
3. Hover over the element you want to either add an exception to OR unblock.
4. Click either **Add exception** or **Remove rule**.
</section>

<section class="platform-firefox" markdown="1">
## Firefox

### How to view a blocked element {: #firefox-view }

1. From the Firefox toolbar, click the **Firefox menu** icon.
2. Click **Web Developer**.
3. Click **Inspector**.
4. Select the **Adblock Plus** tab.

### How to add an exception OR unblock an element {: #firefox-unblock }

1. Follow the steps below *How to view a blocked element*.
2. From the drop-down menu, select **blocked**.
3. Hover over the element you want to either add an exception to OR unblock.
4. Click either **Add exception** or **Remove rule**.
</section>

<section class="platform-opera" markdown="1">
## Opera

### How to view a blocked element {: #opera-view }

#### Windows

1. Click the **Opera** icon, hover over **Developer** and select **Developer Tools**.
2. Click the **>>** icon in the upper right corner and select **Adblock Plus**.

#### Mac

1. From the Mac menu bar, click **Developer** and select **Developer Tools**.
2. Click the **>>** icon in the upper right corner and select **Adblock Plus**.

### How to add an exception rule OR unblock an element {: #opera-unblock }

1. Follow the steps below *How to view a blocked element*.
2. From the drop-down menu, select **blocked**.
3. Hover over the element you want to either add an exception to OR unblock.
4. Click either **Add exception** or **Remove rule**.
</section>

<section class="platform-yandexbrowser" markdown="1">
## Yandex Browser

### How to view a blocked element {: #yandex-view }

1. From the Yandex Browser toolbar, click the **Yandex Browser menu** icon.
2. Hover over **Advanced**, hover over **More Tools** and select **Developer Tools**.
3. Click the **>>** icon in the upper right corner and select **Adblock Plus**.

### How to add an exception OR unblock an element {: #yandex-unblock }

1. Follow the steps below *How to view a blocked element*.
2. From the drop-down menu, select **blocked**.
3. Hover over the element you want to either add an exception to OR unblock.
4. Click either **Add exception** or **Remove rule**.
</section>