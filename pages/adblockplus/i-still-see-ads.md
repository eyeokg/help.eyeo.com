title=I still see ads
description=If you still see ads after installing Adblock Plus, you may need to adjust your settings or verify your filter lists.
template=article
product_id=abp
category=Troubleshooting & Reporting
popular=true
include_heading_level=h3

Follow the suggestions below if you still see ads after installing Adblock Plus.

## Make sure Adblock Plus is turned on {: #abp-enabled }

<section class="platform-chrome" markdown="1">
### Chrome

1. From the Chrome toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extensions**.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Extensions* tab opens.
2. Verify that Adblock Plus is toggled **on**.
3. Close the tab.
</section>

<section class="platform-edge" markdown="1">
### Edge

1. From the Edge toolbar, click the **Edge menu** icon and select **Extensions**.
The *Extensions* tab opens.
2. Verify that Adblock Plus is toggled **on**.
3. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
### Firefox

1. From the Firefox toolbar, click the **Firefox menu** icon and select **Add-ons**.
2. Select **Extensions** from the left menu.
3. Locate Adblock Plus and verify that it is not turned off.
4. Close the tab.
</section>

<section class="platform-msie" markdown="1">
### Internet Explorer

1. Click **Tools** in the upper right corner of the Internet Explorer toolbar.
<br>The *Manage Add-ons* window opens.
2. Select **Manage Add-ons**.
3. Locate and select Adblock Plus.
4. Verify that the status reads **Enabled**.
5. Close the window.
</section>

<section class="platform-opera" markdown="1">
### Opera

1. From the Opera toolbar, right-click (Windows) / control-click (Mac) the **Adblock Plus** icon and select **Manage extension**.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Extensions* tab opens.
2. Locate Adblock Plus and verify that it is turned on.
3. Close the tab.
</section>

<section class="platform-safari" markdown="1">
### Safari

1. From the Mac menu bar, click **Safari** and select **Preferences**.
<br>The *Preferences* window opens.
2. Select the **Extensions** tab.
3. From the left menu, locate Adblock Plus and verify that the check box next to the Adblock Plus icon is selected.
</section>

<section class="platform-yandexbrowser" markdown="1">
### Yandex Browser

1. From the Yandex Browser toolbar, click the **Yandex Browser menu** icon and select **Extensions**.
<br>The *Extensions* tab opens.
2. Select **Extensions**.
3. Locate Adblock Plus and verify that it is toggled on.
4. Close the tab.
</section>

## Manually update all filter lists {: #manual-update }

<section class="platform-chrome" markdown="1">
### Chrome

1. From the Chrome toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-msedge" markdown="1">
### Edge

1. From the Edge toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
<br>The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-firefox" markdown="1">
### Firefox

1. From the Firefox toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-opera" markdown="1">
### Opera

1. From the Opera toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

<section class="platform-yandexbrowser" markdown="1">
### Yandex Browser

1. From the Yandex Browser toolbar, click the **Adblock Plus** icon and then click the **gear** icon in the upper-right corner.
<aside class="alert info" markdown="1">
**Tip**: [What if I don't see the Adblock Plus icon?](adblockplus/hide-the-adblock-plus-icon)
</aside>
The *Adblock Plus Settings* tab opens.
2. Select the *Advanced* tab and scroll to the *Filter Lists* section.
3. Click **Update all filter lists**.
<br>The update is successful if *Just now* appears next to the list.
4. Close the tab.
</section>

## Verify that you are using the correct filter list {: #verify-filter-list }

Adblock Plus requires filter lists to block ads. A default filter list based on your browser's language settings is automatically activated when you install Adblock Plus. It blocks ads from the most popular (often English language-based) websites. However, it does not block ads on less popular national websites. For example, if you live in Germany you should subscribe to the national German filter list.

[View actively maintained filter lists](https://adblockplus.org/en/subscriptions)

<aside class="alert info" markdown="1">
**Tip**: As a recommendation, you should avoid adding too many filter lists to Adblock Plus. Adding too many filter lists slows down the ad blocker and, therefore, your browsing.
</aside>

## Make sure the website is not whitelisted {: #whitelisted }

<? include adblockplus/removewhitelist ?>

## Turn off Acceptable Ads {: #turn-off-aa }

Some nonintrusive ads are displayed by default. If you do not want to see these ads, this feature must be turned off. Please note that by turning off Acceptable Ads, website owners and content creators that abide by the Acceptable Ads criteria will lose ad revenue.

<? include adblockplus/blockads ?>

## Check to see if your computer is infected with adware/malware {: #adware-malware }

### What is adware? {: #what-is-adware }

Adware is a type of malware, short for "malicious software." It's a term generally used for software installed on your computer that is designed to infiltrate or damage a computer system without your consent. In many cases you obtained this software without knowing it, as these applications are often bundled in other software installers.

For information about detecting and removing malware, visit our [adware information page](https://adblockplus.org/adware).

## But I *still* see ads! {: #i-still-see-ads }

If you tried everything above but still see ads, there might be an issue with the filter(s). 

<section class="platform-chrome platform-firefox platform-msedge platform-opera platform-yandexbrowser" markdown="1">
To help rectify the issue, it is recommended that you report the issue via the [Adblock Plus Issue Reporter](adblockplus/report-an-issue).

<aside class="alert info" markdown="1">
**Tip**: Be sure to select the **I still see ads** option within the Issue Reporter.
</aside>

</section>

<section class="platform-msie platform-safari" markdown="1">
[Report the website(s) in the forum](https://adblockplus.org/forum/)
</section>


