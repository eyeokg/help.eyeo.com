title=Adding a browser language
description=Add a preferred language to Adblock Browser for Android.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Languages**.
4. Tap **Add language**.
<br>A list of available languages appears.
5. Tap a language.
<br>The new language is added to your list of preferred languages.
