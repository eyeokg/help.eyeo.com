title=Clearing browsing data
description=Clear your Adblock Browser browsing history, downloads, form and search history, cookies and active logins, saved passwords, cache, offline website data and site settings.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

1. Open the Adblock Browser app.
2. Tap the **Android menu icon** and select **Settings**.
3. Tap **Passwords**.
4. Tap **Clear browsing data**.
5. Tap the *Time range* drop-down menu to select a timeframe.
6. From the *Basic* tab, the following items are selected by default. Tap a specific option if you do not want to clear its associated data.
  - Browsing history
  - Cookies, media licenses, and site data
  - Cached images and files
7. From the *Advanced* tab, choose to clear the following items: 
  - Browsing history (selected by default)
  - Cookies and site data (selected by default)
  - Media licenses
  - Cached images and files (selected by default)
  - Saved passwords
  - Autofill form data
  - Site settings
8. Tap **Clear data**.
