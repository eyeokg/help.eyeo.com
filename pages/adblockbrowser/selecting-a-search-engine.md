title=Selecting a search engine
description=Customize Adblock Browser by choosing from a number of search engines.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

There are many ways to customize Adblock Browser. Start by choosing your preferred search engine.

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Search engine**.
4. Tap the search engine that you want to use.
