title=Removing a filter list
description=Remove a filter list that you previously added to Adblock Browser.
template=article
hide_browser_selector=1
product_id=abb
category=Customization & Settings

You may want to remove a filter list to unblock certain items, or because the filter list is no longer relevant to your browsing habits.

1. Open the Adblock Browser app.
2. Tap the **Android menu** icon and select **Settings**.
3. Tap **Ad blocking**.
4. Tap **Filter lists**.
5. Clear the check box of the filter list that you want to remove.
6. Tap **OK**.
