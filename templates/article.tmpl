{% extends "templates/minimal" %}

{% set product = products[product_id] %}
{% set title_suffix = product.full_name + " Help Center" %}

{% block body %}
<nav aria-label="{{ "Breadcrumb" | translate("breadcrumb-label", "Label") }}" class="breadcrumb">
  <ol class="container large-desktop-width clearfix" itemscope itemtype="http://schema.org/BreadcrumbList">
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="{{ product.slug }}">
        <span itemprop="name">{{ product.full_name | translate(product_id+"-name", "Product name") }}</span>
      </a>
      <meta itemprop="position" content="1" />
    </li>
    <li itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
      <a itemscope itemtype="http://schema.org/Thing" itemprop="item" href="{{ page }}" aria-current="page">
        <span itemprop="name">{{ title | translate(get_page_name(page) + "-title", "Article title") }}</span>
      </a>
      <meta itemprop="position" content="2" />
    </li>
  </ol>
</nav>

<main id="main" class="container clearfix large-desktop-width">
  <div class="row">
    <div class="column two-thirds">
      <article class="article card {{ product_id }}-card section">
        <header class="article-heading">
          <h1 class="has-pre-icon">
            <span class="pre-icon" style="{{ get_inline_bg('logo-'+product_id) }}"></span>
            {{ title | translate( get_page_name(page) + "-title", "Article title") }}
          </h1>
        </header>

        {% if hide_browser_selector is not defined %}
        <div class="article-browser-selector">
          <? include browser-select ?>
        </div>

        <div id="no-content-for-platform-notice" class="content" hidden>
          <p>{{ "Oops! There is no article for the subject and browser that you have selected. Please find a list of browsers that this article is available for below." | translate("no-content-for-platform-notice", "notice") }}</p>
          <ul>
            {% for browser in browsers %}
            <li data-value="{{ browser.id }}">
              <button type="button" class="button-link">
                {{ browser.name | translate(browser.id + "-name") }}
              </button>
            </li>
            {% endfor %}
          </ul>
        </div>
        {% endif %}

        <div class="article-body content">
            {{ body | safe }}
        </div>
      </article>
    </div>
    <aside class="section column one-third">
      <? include product-topics-accordion ?>
    </aside>
  </div>
</main>
{% endblock %}

{% block footer %}
<? include contact ?>
{% endblock %}

{% block scripts %}
<script src="/dist/js/vendor/bowser.min.js"></script>
{% endblock %}
