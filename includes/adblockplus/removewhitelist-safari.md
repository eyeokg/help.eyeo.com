1. From the Safari toolbar, click the **Adblock Plus** icon and select **Open Adblock Plus**.
<br>The *Adblock Plus Settings* window opens.
2. Select the **Whitelist** tab.
3. If a website that you do not want to view ads on appears in the box, select the website and click **Trash** icon.
4. Close the window.

